package testjwt;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;



public class Test {

    private String secret = "paolo";

    public static void main(String [] args){
        try {
            //cree token
            //generateKey();


            String jwtToken = creaJWT(importKeyPub(), importKeyPriv());
            System.out.println(jwtToken);


            //decrypt
            verificatJWT(jwtToken, importKeyPub(), importKeyPriv());

            //printaChiavi(importKeyPriv(),importKeyPub());

            verificaConUrl(jwtToken, importKeyPriv());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void generateKey() throws JOSEException, IOException {
        RSAKey jwk = new RSAKeyGenerator(2048).keyUse(KeyUse.SIGNATURE).keyID(UUID.randomUUID().toString()).generate();

        System.out.println(jwk.toString());
        System.out.println(jwk.toJSONObject());

        //export su file
        PublicKey pub = jwk.toPublicKey();
        PrivateKey priv = jwk.toPrivateKey();

        FileOutputStream out = new FileOutputStream("privata.key");
        out.write(priv.getEncoded());
        out.close();
        System.out.println("Formato "+priv.getFormat()+" alg: "+priv.getAlgorithm());


        FileOutputStream out2 = new FileOutputStream("pubblica.pub");
        out2.write(pub.getEncoded());
        out2.close();
        System.out.println("Formato "+pub.getFormat()+" alg: "+pub.getAlgorithm());


    }


    public static PrivateKey importKeyPriv() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        Path pathPriv = Paths.get("privata.key");
        byte[] bytes = Files.readAllBytes(pathPriv);
        PKCS8EncodedKeySpec kpspec = new PKCS8EncodedKeySpec(bytes);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(kpspec);
    }

    public static PublicKey importKeyPub() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        Path pathPub = Paths.get("pubblica.pub");
        byte[] bytes = Files.readAllBytes(pathPub);
        X509EncodedKeySpec kpspec = new X509EncodedKeySpec(bytes);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(kpspec);
    }

    public static String creaJWT(PublicKey publicKey, PrivateKey privateKey) throws JOSEException {
        Date now = new Date();

        /*
        //per un JWT cifrato e non segnato
        JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder()
                .issuer("Paolo.com")
                .subject("paolo")
                .expirationTime(new Date(now.getTime() + 1000*60*60*12)) //12 ore
                .notBeforeTime(now)
                .issueTime(now)
                .jwtID(UUID.randomUUID().toString())
                .build();

        JWEHeader head = new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128GCM).keyID("a418b851-cd81-42af-a03c-b28dc67ba207").build();
        //JWEHeader head = new JWEHeader(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A128GCM);
        EncryptedJWT jwt = new EncryptedJWT(head,jwtClaims);

        RSAPublicKey pubKeyInterface = (RSAPublicKey) publicKey;
        RSAEncrypter encrypter = new RSAEncrypter(pubKeyInterface);

        jwt.encrypt(encrypter);
        return jwt.serialize();
        */

        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey,(RSAPrivateKey) privateKey);
        String token = JWT.create().withIssuer("Paolo.com")
                .withSubject("paolo")
                .withExpiresAt(new Date(now.getTime() + 1000*60*60*12)) //12 ore
                .withIssuedAt(now)
                .withJWTId(UUID.randomUUID().toString())
                .withKeyId("188290ac-4e21-497d-9816-dc40692b9c8d").withClaim("roles","admin")
                .sign(algorithm);


        return token;

    }

    public static void verificatJWT(String jwtToken, PublicKey publicKey, PrivateKey privateKey) throws ParseException, JOSEException {
        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey,(RSAPrivateKey) privateKey);
        JWTVerifier verifier = JWT.require(algorithm).withIssuer("Paolo.com").build();
        DecodedJWT jwt = verifier.verify(jwtToken);

        System.out.println("Verifica JWT:");
        System.out.println(jwt.getPayload());
        System.out.println(jwt.toString());
    }

    public static void verificaConUrl(String jwtToken, PrivateKey privateKey) throws JwkException, ParseException, MalformedURLException, JOSEException {
        DecodedJWT jwt = JWT.decode(jwtToken);
        JwkProvider provider = new UrlJwkProvider(new URL("http://textclassifier.digitalassistantnow.com/jwks.json"));
        Jwk jwk = provider.get("188290ac-4e21-497d-9816-dc40692b9c8d");
        JWTVerifier verifier = JWT.require(Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), (RSAPrivateKey) privateKey)).build();
        verifier.verify(jwtToken);
    }

}
